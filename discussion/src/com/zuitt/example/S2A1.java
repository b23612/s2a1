package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class S2A1 {
    public static void main(String[] args){

        Scanner yearScanner = new Scanner(System.in);
        System.out.println("Enter a year: ");
        int year = yearScanner.nextInt();

        if ((year%4 == 0 && year%100 != 0) || (year%400 == 0)){
            System.out.println(year + " is a leap year!");
        }
        else {
            System.out.println(year + " is a NOT leap year!");
        }

        int[] intArray = new int[5];

        intArray[0] = 2;
        intArray[1] = 3;
        intArray[2] = 5;
        intArray[3] = 7;
        intArray[4] = 11;

        System.out.println("The first prime number is: " + intArray[0]);

        ArrayList<String> friends = new ArrayList<String>();

        friends.add("John");
        friends.add("Jane");
        friends.add("Chloe");
        friends.add("Zoey");

        System.out.println("My friends are: " + friends);

        HashMap<String, Integer> toiletries = new HashMap<String, Integer>();

        toiletries.put("toothpaste", 15);
        toiletries.put("toothbrush", 20);
        toiletries.put("soap", 12);

        System.out.println("Our current inventory consists of: " + toiletries);


    }
}
