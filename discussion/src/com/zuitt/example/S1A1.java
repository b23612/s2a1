package com.zuitt.example;
import java.util.Scanner;

public class S1A1 {
    public static void main(String[] args){
        Scanner dataInput = new Scanner(System.in);
        System.out.println("First Name: ");
        String firstName = dataInput.nextLine();

        System.out.println("Last Name: ");
        String lastName = dataInput.nextLine();

        System.out.println("First Subject Grade: ");
        double firstGrade = dataInput.nextDouble();

        System.out.println("Second Subject Grade: ");
        double secondGrade = dataInput.nextDouble();

        System.out.println("Third Subject Grade: ");
        double thirdGrade = dataInput.nextDouble();

        double average = (firstGrade + secondGrade + thirdGrade)/3;
        System.out.println("Good day, " + firstName + " " + lastName + "!");
        System.out.println("Your grade average is: " + Math.round(average*100.00)/100.00);

    }
}
